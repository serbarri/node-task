# Markdown example

This project contains node
hello-world

## Installation steps

1. Copy the repository
2. Install dependecies
3. Start the app

## Terminal command

Code block:

```sh
git clone https://gitlab.com/serbarri/fork-repo.git
cd ./node-task
npm i
```
Start cmd:` npm run start`

## App should work
![alt text for text image](https://docs.microsoft.com/en-us/windows/terminal/images/overview.png "text")
```
{
  "firstName": "John",
  "lastName": "Smith",
  "age": 25
}
```

