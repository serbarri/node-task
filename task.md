# SORTING ALGORITHMS

In this document we will see some algorithms used for sorting arrays. We are going to see the next ones:

- **Buble sort**
- **Insertion sort**
- **Selection sort**
  


## 1. Bubble sort

 It works by checking each item in the list to be sorted with the next, swapping them if they are in the wrong order. For example, if we are sorting the array from higest to lowest, and we compare two numbers of it, if the first one is less than the second one, we swap the values, otherwise we continue with the rest of the array. 
 
 The entire list needs to be reviewed several times until no more exchanges are needed, which means the list is ordered. This algorithm gets its name from the way items move up the list during exchanges, like little "bubbles." It is also known as the direct exchange method. Since it only uses comparisons to operate elements, it is considered a comparison algorithm, being one of the simplest to implement.

 ![alt text for text image](https://upload.wikimedia.org/wikipedia/commons/c/c8/Bubble-sort-example-300px.gif)

 ### Implementation  
 ---
Here we will see the implementation of this algorithm and an auxiliar function which swaps the numbers between two positions of the array we want to sort. 
 ```js
    function selectSort(myArray) {
        var size = myArray.length;
        for (var temp = 0; temp < size - 1; temp++) {
            mayor = temp;
            for (var check = temp + 1; check < size; check++) {
                if (myArray[check] < myArray[mayor]) {
                    mayor = check;
                }
            }
            sorting(myArray, mayor, check);
        } return myArray;
    }
```

```js
function sorting(myArray, value1, value2) {
    if (value1 == value2) {
        return myArray;
    }
    var temp = myArray[value1];
    myArray[value1] = myArray[value2];
    myArray[value2] = temp;
    return myArray;
}
```

## 2. Insertion sort
Insertion sort iterates, consuming one input element each repetition, and growing a sorted output list. At each iteration, insertion sort removes one element from the input data, finds the location it belongs within the sorted list, and inserts it there. It repeats until no input elements remain.

Sorting is typically by iterating up the array, growing the sorted list behind it. At each array-position, it checks the value there against the largest value in the sorted list . If larger, it leaves the element in place and moves to the next. If smaller, it finds the correct position within the sorted list, shifts all the larger values up to make a space, and inserts into that correct position.

 ![Insertion sort algorithm animation](https://upload.wikimedia.org/wikipedia/commons/0/0f/Insertion-sort-example-300px.gif)


### Implementation
---
```js
function insertionSort(myArray) {
    var tam = myArray.length, temp, lugar;
    for (var obj = 0; obj < tam; obj++) {
        temp = myArray[obj];
        for (lugar = obj - 1; lugar >= 0 && myArray[lugar] > temp; lugar--) {
            myArray[lugar + 1] = myArray[lugar];
        }
        myArray[lugar + 1] = temp;
    }
    return myArray;
}
```


## 3. Selection sort
The selection sort algorithm sorts an array by repeatedly finding the minimum element (considering ascending order) from unsorted part and putting it at the beginning. The algorithm maintains two subarrays in a given array.

1) The subarray which is already sorted.
2) Remaining subarray which is unsorted.

In every iteration of selection sort, the minimum element (considering ascending order) from the unsorted subarray is picked and moved to the sorted subarray.

![Selection sort algorithm animation](https://upload.wikimedia.org/wikipedia/commons/9/94/Selection-Sort-Animation.gif)

### Implementation
---
```js
function selectSort(myArray) {
    var n = myArray.length;

    for (var i = 0; i < n; i++) {
        var min = i;
        for (var j = i + 1; j < n; j++) {
            if (myArray[j] < myArray[min]) {
                min = j;
            }
        }
        if (min != i) {
            sorting(myArray, i, min);

        }
    }
    return myArray;
}

```

## Algorithms comparision
 These algoritms have differents temporal costs. To see this better, we will show you a table to see which one of these is better in terms of temporal cost, comparing it in the wors and in the best case. 

 | Algorithm | Best case | Worst case
| ----------- | ----------- | ----------- |
| Bubble sort | θ (n<sup>2</sup>)  | θ (n<sup>2</sup>) |
| Selection sort | Θ (n<sup>2</sup>) |Θ (n<sup>2</sup>)  |
| Insertion sort | Ω (n) | O (n<sup>2</sup>) |


### Conclusion
---

As we can see, buble and selection sort always have the same temporal cost because these algoritms are based in two nested loops that will be done independently of the array state. So that, in both cases the cost remains constant. On the other hand, insertion sort improves the other algorithms in the best case. 

## Summary
---
In this post we have seen different sorting algorithms: selection, insertion and bubble sort. We have explained what they consist in and we've seen that the best one of them attending to the temporal cost is the insertion one, but only in the best case. 

I let the code in a file in order you can test them by yourself.
I hope you have enjoyed! Below I let you too a list of tasks to complete that will help you a lot to understand what we have seen if you do them. 

### **List of tasks to do**
- [x] To introduce you to sort algoritms
- [x] To compare temporal costs 
- [ ] To test the code

## Annex: Execution of the program
---
The file *taskApp.js* contains the implementation of the algorithms which we've seen before and an unsorted array to check that these works correctly. To execute it, you have to type this command inside the node-task folder:

```sh
npm run start
```

## References
---
1. [Insertion sort](https://www.geeksforgeeks.org/insertion-sort/)
2. [Sorting algorithms](https://www.solvetic.com/tutoriales/article/2181-algoritmos-sencillos-de-ordenacion-en-javascript/#:~:text=Los%203%20algoritmos%20de%20ordenaci%C3%B3n,un%20n%C3%BAmero%20n%20de%20veces.)
3. [Selection sort: programiz.com](https://www.programiz.com/dsa/selection-sort)
4. [Buble sort](https://es.wikipedia.org/wiki/Ordenamiento_de_burbuja)





