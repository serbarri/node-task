const express = require('express')
const app = express()
const port = 3000

/**
 * This function sorts an array by using the buble algorithm
 * @param {Number[]} array  The array of numbers we want to sort
 * @returns {Number[]} The array returned is sorted when function finishes
 */

function bubleSort(array) {
    var myArray = array.slice(0, array.length + 1);
    var size = myArray.length;
    for (var tem = 1; tem < size; tem++) {
        for (var left = 0; left < (size - tem); left++) {
            var right = left + 1;
            if (myArray[left] > myArray[right]) {
                sorting(myArray, left, right);
            }
        }
    }
    return myArray;
}

/**
 * This function takes an array and swap the values which are in the indexes value1 and value2.
 * The method is used by bubleSort function
 * @param {Number[]} myArray The array in which the swap will be made
 * @param {Number} value1 First index of myArray used to make the swap
 * @param {Number} value2 Second index of myArray used to make the swap
 * @returns {Number[]} The array returned is sorted in the indexes where we have made the swap when the function finishes
 */

function sorting(myArray, value1, value2) {
    if (value1 == value2) {
        return myArray;
    }
    var temp = myArray[value1];
    myArray[value1] = myArray[value2];
    myArray[value2] = temp;
    return myArray;
}


/**
 * This function sorts an array by using the selection algorithm
 * @param {Number[]} array 
 * @returns {Number[]} The array returned is sorted when the function finishes
 */


function selectSort(array) {
    var myArray = array.slice(0, array.length + 1);
    var n = myArray.length;

    for (var i = 0; i < n; i++) {
        // Finding the smallest number in the subarray
        var min = i;
        for (var j = i + 1; j < n; j++) {
            if (myArray[j] < myArray[min]) {
                min = j;
            }
        }
        if (min != i) {
            sorting(myArray, i, min);

        }
    }
    return myArray;
}
/**
 * This function sorts an array by using the insertion algorithm
 * @param {Number[]} array The array we want to sort
 * @returns {Number[]} The array returned is sorted when the function finishes
 */
function insertionSort(array) {
    var myArray = array.slice(0, array.length + 1);
    var length = myArray.length;
    for (var i = 1; i < length; i++) {
        var key = myArray[i];
        var j = i - 1;
        while (j >= 0 && myArray[j] > key) {
            myArray[j + 1] = myArray[j];
            j = j - 1;
        }
        myArray[j + 1] = key;
    }
    return myArray;
}

var numbers = [8, 10, 2, 6, 3];

app.get('/', (req, res) => {
    
    res.send('SORTING ALGORITHMS');
    

})
app.get('/buble', (req, res) => {
    var bubleArray = bubleSort(numbers);
    console.log("Unsort array: " + numbers);
    console.log("Sort array using buble sort: " + bubleArray);
    res.send(`Sorted array using buble sort: ${bubleArray}`);
})

app.get('/select', (req, res) => {
    var selectArray = selectSort(numbers);
    console.log("Unsort array: " + numbers);
    console.log("Sort array using buble sort: " + selectArray);
    res.send(`Sorted array using selection sort: ${selectArray}`);
})

app.get('/insert', (req, res) => {
    var insertArray = insertionSort(numbers);
    console.log("Unsort array: " + numbers);
    console.log("Sort array using insertion sort: " + insertArray);
    res.send(`Sorted array using insertion sort: ${insertArray}`);
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})